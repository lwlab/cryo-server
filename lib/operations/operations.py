# This file includes all of the operations.

from generic_operation import *
from mag_up import *
from mag_down import *
from begin_magnetization import *
from begin_regulation import *
from sleep_operation import *
from change_regulation import *
from emergency_shutdown import *